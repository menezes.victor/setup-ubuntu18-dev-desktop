import os


with open('/etc/passwd', 'r') as f:
    users_info = f.readlines()

for line in users_info:
    user_info = line.split(':')
    username = user_info[0]
    uid = int(user_info[2])
    gid = int(user_info[3])
    home_dir = user_info[5]

    add_workon_home_line = [True,
                            'export WORKON_HOME={}/venvs'.format(home_dir)]
    add_venv_wrapper_line = [True,
                             'export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3']
    add_load_venv_line = [True, 'source /usr/local/bin/virtualenvwrapper.sh']
    if home_dir.startswith('/home') and os.path.exists(home_dir):
        venv_dir = os.path.join(home_dir, 'venvs')
        if not os.path.exists(venv_dir):
            os.mkdir(venv_dir)
            os.chown(venv_dir, uid, gid)

        bashrc_path = os.path.join(home_dir, '.bashrc')

        with open(bashrc_path, 'r') as f:
            for line in f.readlines():
                if add_workon_home_line[1] in line:
                    add_workon_home_line[0] = False
                if add_venv_wrapper_line[1] in line:
                    add_venv_wrapper_line[0] = False
                if add_load_venv_line[1] in line:
                    add_load_venv_line[0] = False

        with open(bashrc_path, 'a') as f:
            if add_workon_home_line[0]:
                f.write(add_workon_home_line[1] + '\n')
            if add_venv_wrapper_line[0]:
                f.write(add_venv_wrapper_line[1] + '\n')
            if add_load_venv_line[0]:
                f.write(add_load_venv_line[1] + '\n')
