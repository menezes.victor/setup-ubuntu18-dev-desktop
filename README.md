# Install basic tools for Ubuntu18.04 desktop developer machine.

To Use, edit `username` on `playbook.yml` to match your username.
While you have the `playbook.yml` open, feel free to read the `roles` section
and comment-out any application you don't want installed.

Also, help yourself at the `roles/` folder to inspect what each `role` from
the playbook actually does under the hood.

```
sudo apt install ansible

mkdir ~/projects
cd projects

git clone git@gitlab.com:menezes.victor/setup-ubuntu18-dev-desktop.git
cd setup-ubuntu18-dev-desktop

sudo ansible-playbook playbook.yml
```

Thanks
VM
menezes.victor@gmail.com
